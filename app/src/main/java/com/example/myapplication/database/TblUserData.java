package com.example.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TblUserData extends MyDatabase {
    public static final String TABLE_NAME="Tbl_UserData";
    public static final String USER_ID="UserId";
    public static final String USER_NAME="Name";
    public static final String USER_EMAIL="Email";
    public static final String USER_PASSWORD="Password";
    public static final String GENDER="Gender";
    public static final String HOBIE="Hobie";

    public TblUserData(Context context) {
        super(context);
    }
    public long insertUserDetails(String name, String email, String password,String gender,String hobie) {

        SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(USER_NAME, name);
            cv.put(USER_EMAIL, email);
            cv.put(USER_PASSWORD, password);
            cv.put(GENDER,gender);
            cv.put(HOBIE,hobie);
            long insertedID = db.insert(TABLE_NAME, null, cv);
            db.close();
            return insertedID;
    }

}
