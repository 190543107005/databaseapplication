package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.myapplication.database.MyDatabase;
import com.example.myapplication.database.TblUserData;

public class MainActivity extends AppCompatActivity {
    EditText NAME1,EMAIL1,PASSWORD1,HOBIE1;
    Button SUBMIT,View1;
    RadioGroup radioGroup;
    RadioButton getRadioButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SUBMIT=findViewById(R.id.Submit);
        View1=findViewById(R.id.VIEW);
        NAME1 = findViewById(R.id.etName);
        EMAIL1 = findViewById(R.id.etEmail);
        PASSWORD1 = findViewById(R.id.etPassword);
        radioGroup=findViewById(R.id.gender);
        HOBIE1=findViewById(R.id.Hobies);

        new MyDatabase(MainActivity.this).getReadableDatabase();
        check();
    }
    public void check() {
        {
            SUBMIT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String NaMe = NAME1.getText().toString();
                    String EMail = EMAIL1.getText().toString();
                    String PAssword = PASSWORD1.getText().toString();
                    String radioButton=getRadioButton.getText().toString();
                    String HObie=HOBIE1.getText().toString();
                TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastinsertedID = tblUserData.insertUserDetails(NaMe, EMail, PAssword,radioButton,HObie);
                    Toast.makeText(getApplicationContext(), lastinsertedID > 0 ? "Inserted Successfully" : "Something went Wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    public void checkButton(View v)
    {
        int radio=radioGroup.getCheckedRadioButtonId();
        getRadioButton=findViewById(radio);
    }
}